//KEY FOR LOCK:2134
int count=0;
int flag=0;
void setup() {
  // put your setup code here, to run once:
 //3,4,5,6 FOR EACH STAGE & 7 FOR WRONG INPUT
  pinMode(A0,INPUT);
  pinMode(3,OUTPUT);
  pinMode(4,OUTPUT);
  pinMode(5,OUTPUT);
  pinMode(6,OUTPUT);

  pinMode(7,OUTPUT);
 

  Serial.begin(9600);
  //pinMode(4,OUTPUT);
}

void loop() {
  
  float ans=analogRead(A0);
  float val=(ans*5)/1023;
  if(count==0)
  {
    if(val>=1.25 && val<2.5)
    {
      digitalWrite(3,HIGH);
      digitalWrite(7,LOW); 
    }
    else
    {
       digitalWrite(7,HIGH);
       digitalWrite(3,LOW);
       digitalWrite(4,LOW);
       digitalWrite(5,LOW);
       digitalWrite(6,LOW);
       flag=1;
    }
  }
  else if(count==1)
  {
    if(val>=0 && val<1.25)
    {
      digitalWrite(4,HIGH);
      digitalWrite(7,LOW);
    }
    else
    {
       digitalWrite(7,HIGH);
       digitalWrite(3,LOW);
       digitalWrite(4,LOW);
       digitalWrite(5,LOW);
       digitalWrite(6,LOW);
       flag=1;
    }  
  }
  else if(count==2)
  {
    if(val>=2.5 && val<3.75)
    {
       digitalWrite(5,HIGH);
       digitalWrite(7,LOW);
    }
    else
    {
       digitalWrite(7,HIGH);
       digitalWrite(3,LOW);
       digitalWrite(4,LOW);
       digitalWrite(5,LOW);
       digitalWrite(6,LOW);
       flag=1;
    }
  }
  else if(count==3)
  {
    if(val>=3.75 && val<=5)
    {
       digitalWrite(6,HIGH);
       digitalWrite(7,LOW);
    }
    else
    {
       digitalWrite(7,HIGH);
       digitalWrite(3,LOW);
       digitalWrite(4,LOW);
       digitalWrite(5,LOW);
       digitalWrite(6,LOW);
       flag=1;
    }
  }

//  Serial.println(count);
  Serial.println(val);
  Serial.println("===============");
  
  if(flag==1)
  {
    count=0;
    flag=0;
  }
  else
  {
    count=(count+1)%4;
  }

  
  delay(6000);
  // put your main code here, to run repeatedly:
}
