/*LED IS HIGH WHEN PUSH BUTTON IS PRESSED*/
#include<avr/io.h>
#include<avr/delay.h>
#include<avr/io.h> // include basic input output library
int main()
{
  //Input pin=2 D, Output pin=9 B
  DDRD &= ~0x04;
  DDRB |= 0x02;
// this is safer as it sets appropriate pins of PORTB as outputs
// without changing the value of pins 0 & 1 DDRD = DDRD | B11111100;
// This is safer as it sets pins 2 to 7 as outputs
// without changing the value of pins 0 & 1

while(1)
{
  int ip=(PIND & 0x04);
  if(ip)
  {
    if((PINB&0x02)==0x02)
    {
      PORTB &= ~0x02;
      _delay_ms(5);
    }
    else
    {
      PORTB |= 0x02;
      _delay_ms(5);
    }      
  }
// turn the LED on (HIGH is the voltage level)
//_delay_ms(1000);
// wait for a second
// turn the LED off by making the voltage LOW
//_delay_ms(1000);
// wait for a second
}
}
