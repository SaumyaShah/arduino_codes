void setup() {
  // put your setup code here, to run once:
  pinMode(6,OUTPUT);
  pinMode(A0,INPUT);
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  float vin=analogRead(A0);
  Serial.println(vin);
  float inp=(vin*255)/1023;
  analogWrite(6,inp);
  Serial.println(inp);
  Serial.println("===============================");
  delay(1000);
}
